package dsl1

// initial, object-based implementation
class Test1 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            // CONFIGURATION
            val doorClosed = Event("doorClosed", "D1CL")
            val drawerOpened = Event("drawerOpened", "D2OP")
            val lightOn = Event("lightOn", "L1ON")
            val doorOpened = Event("doorOpened", "D1OP")
            val panelClosed = Event("panelClosed", "PNCL")

            val unlockPanelCommand = Command("unlockPanel", "PNUL")
            val lockPanelCommand = Command("lockPanel", "PNLK")
            val lockDoorCommand = Command("lockDoor", "D1LK")
            val unlockDoorCommand = Command("unlockDoor", "D1UL")

            val idleState = State()
            val activeState = State()
            val waitingForLightState = State()
            val waitingForDrawerState = State()
            val unlockedPanelState = State()

            val machine = StateMachine(idleState)

            idleState.transitions[doorClosed] = activeState
            idleState.actions.add(unlockDoorCommand)
            idleState.actions.add(lockPanelCommand)

            activeState.transitions[drawerOpened] = waitingForLightState
            activeState.transitions[lightOn] = waitingForDrawerState

            waitingForLightState.transitions[lightOn] = unlockedPanelState

            waitingForDrawerState.transitions[drawerOpened] = unlockedPanelState

            unlockedPanelState.actions.add(unlockPanelCommand)
            unlockedPanelState.actions.add(lockDoorCommand)
            unlockedPanelState.transitions[panelClosed] = idleState

            machine.resetEvents.add(doorOpened)

            // test run
            val controller = Controller(idleState, machine, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(doorClosed)
            controller.handle(drawerOpened)
            controller.handle(lightOn)
            controller.handle(panelClosed)

            println()
            println("TEST 2")
            println("======")
            controller.handle(doorClosed)
            controller.handle(lightOn)
            controller.handle(drawerOpened)
            controller.handle(panelClosed)

            println()
            println("TEST 3")
            println("======")
            controller.handle(doorClosed)
            controller.handle(doorOpened)

            println()
            println("TEST 4")
            println("======")
            controller.handle(doorClosed)
            controller.handle(lightOn)
            controller.handle(doorOpened)

            println()
            println("TEST 5")
            println("======")
            controller.handle(doorClosed)
            controller.handle(lightOn)
            controller.handle(drawerOpened)
            controller.handle(doorOpened)
        }
    }
}