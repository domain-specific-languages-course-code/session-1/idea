package dsl1

import java.io.File
import javax.xml.parsers.DocumentBuilderFactory

// NOTE - no error checking here, assumes everything is correct, and I chose the evil DOM API to avoid external deps
class XmlConfigurator(private val fileName: String) {
    fun configure() : StateMachine {
        val events = mutableMapOf<String, Event>()
        val commands = mutableMapOf<String, Command>()
        val states = mutableMapOf<String, State>()

        val document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(File(fileName))
        val root = document.documentElement
        val startStateName = root.attributes.getNamedItem("start").textContent
        val eventNodes = root.getElementsByTagName("event")
        val commandNodes = root.getElementsByTagName("command")
        val stateNodes = root.getElementsByTagName("state")

        for(i in 0 until eventNodes.length) {
            val attributes = eventNodes.item(i).attributes
            val name = attributes.getNamedItem("name").textContent
            val code = attributes.getNamedItem("code").textContent
            events[name] = Event(name, code)
        }

        for(i in 0 until commandNodes.length) {
            val attributes = commandNodes.item(i).attributes
            val name = attributes.getNamedItem("name").textContent
            val code = attributes.getNamedItem("code").textContent
            commands[name] = Command(name, code)
        }

        // pass 1 over states just to register them
        for(i in 0 until stateNodes.length) {
            val node = stateNodes.item(i)
            val name = node.attributes.getNamedItem("name").textContent
            val state = State()
            states[name] = state
        }

        // pass 2 over states to setup transitions
        for(i in 0 until stateNodes.length) {
            val node = stateNodes.item(i)
            val name = node.attributes.getNamedItem("name").textContent
            val state = states[name]
            val childNodes = node.childNodes
            for(j in 0 until childNodes.length) {
                val childNode = childNodes.item(j)
                when (childNode.nodeName) {
                    "transition" -> {
                        val event = childNode.attributes.getNamedItem("event").textContent
                        val target = childNode.attributes.getNamedItem("target").textContent
                        state!!.transitions[events[event]!!] = states[target]!!
                    }
                    "action" -> {
                        val commandName = childNode.attributes.getNamedItem("command").textContent
                        state!!.actions.add(commands[commandName]!!)
                    }
                }
            }
        }

        val machine = StateMachine(states[startStateName]!!)
        events.forEach {
            machine.allEvents[it.key] = it.value
        }
        states.forEach {
            machine.allStates[it.key] = it.value
        }

        val resetEventNodes = root.getElementsByTagName("resetEvent")
        for(i in 0 until resetEventNodes.length) {
            val resetEventName = resetEventNodes.item(i).attributes.getNamedItem("name").textContent
            machine.resetEvents.add(events[resetEventName]!!)
        }

        return machine
    }
}