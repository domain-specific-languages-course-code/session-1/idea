package dsl1

// XML-based configuration, setting up the objects directly
class Test2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            // CONFIGURATION
            val machine = XmlConfigurator("config.xml").configure()

            fun e(name : String) = machine.event(name)
            fun s(name : String) = machine.state(name)

            // test run
            val controller = Controller(s("idle"), machine, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("lightOn"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 2")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 3")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 4")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 5")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("doorOpened"))
        }
    }
}