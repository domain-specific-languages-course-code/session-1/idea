package dsl4

import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.TerminalNode


// Use an ANTLR parser to read a file using a custom configuration language

class Test6 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val input = CharStreams.fromFileName("config.gothic")
            val lexer = GothicSecurityLexer(input)
            val tokens = CommonTokenStream(lexer)
            val parser = GothicSecurityParser(tokens)
            var stateMachine : StateMachine? = null
            var currentState : State? = null
            val stateTransitions = mutableMapOf<State, MutableMap<String, String>>()
            val stateActions = mutableMapOf<State, MutableList<String>>()

            // The parser listener walks the parse tree as it is created, and creates the state machine objects
            parser.addParseListener(object : GothicSecurityListener {

                override fun enterStateMachine(ctx: GothicSecurityParser.StateMachineContext) {
                    stateMachine = StateMachine()
                }
                override fun exitStateMachine(ctx: GothicSecurityParser.StateMachineContext) {
                    stateMachine!!.name = ctx.ID(0).text
                    stateMachine!!.startState = stateMachine!!.state(ctx.ID(1).text)
                    stateTransitions.forEach { state, transitions ->
                        transitions.forEach { eventName, targetName ->
                            state.transitions[stateMachine!!.event(eventName)] = stateMachine!!.state(targetName)
                        }
                    }
                    stateActions.forEach { state, actions ->
                        actions.forEach {
                            state.actions.add(stateMachine!!.command(it))
                        }
                    }
                }

                override fun exitCommand(ctx: GothicSecurityParser.CommandContext) {
                    val name = ctx.ID(0).text
                    val code = ctx.ID(1).text
                    stateMachine!!.commands[name] = Command(name, code)
                }

                override fun exitEvent(ctx: GothicSecurityParser.EventContext) {
                    val name = ctx.ID(0).text
                    val code = ctx.ID(1).text
                    stateMachine!!.events[name] = Event(name, code)
                }

                override fun enterState(ctx: GothicSecurityParser.StateContext) {
                    currentState = State()
                    stateTransitions[currentState!!] = mutableMapOf()
                    stateActions[currentState!!] = mutableListOf()
                }

                override fun exitState(ctx: GothicSecurityParser.StateContext) {
                    val name = ctx.ID().text
                    stateMachine!!.states[name] = currentState!!
                }

                override fun exitAction(ctx: GothicSecurityParser.ActionContext) {
                    stateActions[currentState!!]!!.add(ctx.ID().text)
                }

                override fun exitTransition(ctx: GothicSecurityParser.TransitionContext) {
                    stateTransitions[currentState!!]!![ctx.ID(0).text] = ctx.ID(1).text
                }

                override fun exitResetOn(ctx: GothicSecurityParser.ResetOnContext) {
                    stateMachine!!.resetEvents.add(stateMachine!!.event(ctx.ID().text))
                }

                override fun enterCommand(ctx: GothicSecurityParser.CommandContext) {}
                override fun enterEvent(ctx: GothicSecurityParser.EventContext) {}
                override fun enterTransition(ctx: GothicSecurityParser.TransitionContext) {}
                override fun enterResetOn(ctx: GothicSecurityParser.ResetOnContext) {}
                override fun enterAction(ctx: GothicSecurityParser.ActionContext) {}

                override fun enterEveryRule(ctx: ParserRuleContext) {}
                override fun exitEveryRule(ctx: ParserRuleContext) {}
                override fun visitErrorNode(node: ErrorNode) {
                    println(node)
                }
                override fun visitTerminal(node: TerminalNode) {}
            })

            parser.buildParseTree = true
            val tree = parser.stateMachine()

            println(tree.toStringTree(parser))

            fun e(name : String) = stateMachine!!.event(name)
            fun s(name : String) = stateMachine!!.state(name)

            // test run
            val controller = Controller(s("idle"), stateMachine!!, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("lightOn"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 2")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 3")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 4")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 5")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("doorOpened"))
        }
    }
}