package dsl4

// This example uses ANTLR v4 to crate a parser generator for a configuration language
// The building happens in the ANTLR parser listener
abstract class AbstractEvent(val name : String,
                             val code : String)

class Command(name: String, code: String) : AbstractEvent(name, code)
class Event(name: String, code: String) : AbstractEvent(name, code)

class State {
    var name : String? = null
    val actions = mutableListOf<Command>()
    val transitions = mutableMapOf<Event, State>()
}

class StateMachine {
    var name : String? = null
    var startState : State? = null
    var commands = mutableMapOf<String, Command>()
    var events = mutableMapOf<String, Event>()
    var states = mutableMapOf<String, State>()
    var resetEvents = mutableSetOf<Event>()

    fun command(name : String) = commands[name]!!
    fun event(name : String) = events[name]!!
    fun state(name : String) = states[name]!!

    fun resetTarget(event : Event) =
        if (resetEvents.contains(event))
            startState
        else null
}

class CommandChannel {
    fun send(action: Command) {
        println("${action.name} / ${action.code}")
    }
}

class Controller(private var currentState : State,
                 private val stateMachine : StateMachine,
                 private val commandChannel : CommandChannel) {
    fun handle(event : Event) {
        val nextState =
                currentState.transitions[event] ?:
                stateMachine.resetTarget(event)
        nextState?.let {
            currentState = it
            currentState.actions.forEach { commandChannel.send(it) }
        }
    }
}
