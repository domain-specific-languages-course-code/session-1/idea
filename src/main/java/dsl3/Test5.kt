package dsl3

// Separating out the "builder" from the actual objects
// We added extention methods that take lambdas that use the builders as receivers, allowing us to say
//    command {
//       name = "x"
//       code = "y"
//    }
// which looks more like a declaration than a function call.
// This allows nested setup that looks more like a data structure than function calls

class Test5 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            // CONFIGURATION
            val stateMachine =
                    stateMachine {
                        startState = "idleState"
                        command {
                            name = "unlockPanel"
                            code = "PNUL"
                        }
                        command {
                            name = "lockPanel"
                            code = "PNLK"
                        }
                        command {
                            name = "lockDoor"
                            code = "D1LK"
                        }
                        command {
                            name = "unlockDoor"
                            code = "D1UL"
                        }

                        event {
                            name = "doorClosed"
                            code = "D1CL"
                        }
                        event {
                            name = "drawerOpened"
                            code = "D2OP"
                        }
                        event {
                            name = "lightOn"
                            code = "L1ON"
                        }
                        event {
                            name = "doorOpened"
                            code = "D1OP"
                        }
                        event {
                            name = "panelClosed"
                            code = "PNCL"
                        }

                        state {
                            name = "idleState"
                            action {
                                command = "unlockDoor"
                            }
                            action {
                                command = "lockPanel"
                            }
                            transition {
                                event = "doorClosed"
                                target = "activeState"
                            }
                        }

                        state {
                            name = "activeState"
                            transition {
                                event = "drawerOpened"
                                target = "waitingForLightState"
                            }
                            transition {
                                event = "lightOn"
                                target = "waitingForDrawerState"
                            }
                        }

                        state {
                            name = "waitingForLightState"
                            transition {
                                event = "lightOn"
                                target = "unlockedPanelState"
                            }
                        }

                        state {
                            name = "waitingForDrawerState"
                            transition {
                                event = "drawerOpened"
                                target = "unlockedPanelState"
                            }
                        }

                        state {
                            name = "unlockedPanelState"
                            action {
                                command = "unlockPanel"
                            }
                            action {
                                command = "lockDoor"
                            }
                            transition {
                                event = "panelClosed"
                                target = "idleState"
                            }
                        }

                        resetEvent("doorOpened")
                    }


            fun e(name : String) = stateMachine.event(name)
            fun s(name : String) = stateMachine.state(name)

            // test run
            val controller = Controller(s("idleState"), stateMachine, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("lightOn"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 2")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 3")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 4")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 5")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("doorOpened"))
        }
    }
}