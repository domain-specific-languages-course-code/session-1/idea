package dsl3

// Separating out the "builder" from the actual objects
// We add extention methods that take lambdas that use the builders as receivers, allowing us to say
//    command {
//       name = "x"
//       code = "y"
//    }
// which looks more like a declaration than a function call.
// This allows nested setup that looks more like a data structure than function calls

abstract class AbstractEvent(val name : String,
                             val code : String)

class Command(name: String, code: String) : AbstractEvent(name, code)
class Event(name: String, code: String) : AbstractEvent(name, code)

class State(val name : String,
            val actions : List<Command>) {
    val transitions = mutableMapOf<Event, State>()
}

class StateMachine(val startState : State,
                   val allCommands : Map<String, Command>,
                   val allEvents  : Map<String, Event>,
                   val allStates : Map<String, State>,
                   val resetEvents : Set<Event>) {

    fun command(name : String) = allCommands[name]!!
    fun event(name : String) = allEvents[name]!!
    fun state(name : String) = allStates[name]!!

    fun resetTarget(event : Event) =
        if (resetEvents.contains(event))
            startState
        else null
}

class CommandChannel {
    fun send(action: Command) {
        println("${action.name} / ${action.code}")
    }
}

class Controller(private var currentState : State,
                 private val stateMachine : StateMachine,
                 private val commandChannel : CommandChannel) {
    fun handle(event : Event) {
        val nextState =
                currentState.transitions[event] ?:
                stateMachine.resetTarget(event)
        nextState?.let {
            currentState = it
            currentState.actions.forEach { commandChannel.send(it) }
        }
    }
}

// -------------------------------------------------------------------------
// BUILDER
// -------------------------------------------------------------------------
// Separating out the "builder" from the actual state machine
// The builder gets initialized with the string values then assembles
//    them into actual state objects

open class AbstractEventBuilder {
    var name: String? = null
    var code: String? = null
}

class EventBuilder : AbstractEventBuilder()
class CommandBuilder : AbstractEventBuilder()

class TransitionBuilder {
    var event : String? = null
    var target : String? = null
}
class ActionBuilder {
    var command : String? = null
}

class StateBuilder {
    var name : String? = null
    val actions = mutableListOf<ActionBuilder>()
    val transitions = mutableListOf<TransitionBuilder>()
    fun transition(init : TransitionBuilder.() -> Unit) = TransitionBuilder().apply {
        init()
        transitions.add(this)
    }
    fun action(init : ActionBuilder.() -> Unit) = ActionBuilder().apply {
        init()
        actions.add(this)
    }
}

class StateMachineBuilder {
    var startState : String? = null
    private val commands = mutableListOf<CommandBuilder>()
    private val events  = mutableListOf<EventBuilder>()
    private val states = mutableListOf<StateBuilder>()
    private val resetEvents = mutableListOf<String>()

    fun <T> setup(item : T, list : MutableList<T>, init : T.()->Unit) {
        item.init()
        list.add(item)
    }
    fun event(init : EventBuilder.() -> Unit) = setup(EventBuilder(), events, init)
    fun command(init : CommandBuilder.() -> Unit) = setup(CommandBuilder(), commands, init)
    fun state(init : StateBuilder.() -> Unit) = setup(StateBuilder(), states, init)
    fun resetEvent(name : String) = resetEvents.add(name)

    fun build() : StateMachine {
        val commandMap = commands.map { it.name!! to Command(it.name!!, it.code!!)}.toMap()
        val eventMap = events.map { it.name!! to Event(it.name!!, it.code!!)}.toMap()
        val stateMap = states.map { it.name!! to State(it.name!!, it.actions.map { commandMap[it.command]!! }) }.toMap()
        states.forEach {stateBuilder ->
            val state = stateMap[stateBuilder.name]!!
            stateBuilder.transitions.forEach {transition ->
                state.transitions[eventMap[transition.event]!!] = stateMap[transition.target]!!
            }
        }
        return StateMachine(stateMap[startState]!!, commandMap, eventMap, stateMap, resetEvents.map { eventMap[it]!! }.toSet())
    }
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) = StateMachineBuilder().let {
    it.init()
    it.build()
}

