package dsl2

// change objects to use constructor-based specification, creating a more fluent construction
abstract class AbstractEvent(val name : String,
                             val code : String)

class Command(name: String, code: String)
    : AbstractEvent(name, code)

class Event(name: String, code: String)
    : AbstractEvent(name, code)

class State(val name : String,
            private val actionNames : List<String>,
            private val transitionSpec : Map<String, String>) {
    val actions = mutableListOf<Command>()
    val transitions = mutableMapOf<Event, State>()

    fun resolve(stateMachine: StateMachine) {
        transitionSpec.forEach {
            transitions[stateMachine.allEvents[it.key]!!] = stateMachine.allStates[it.value]!!
        }
        actionNames.forEach {
            actions.add(stateMachine.allCommands[it]!!)
        }
    }
}

class StateMachine(startStateName : String,
                   resetEventNames : Set<String>,
                   commands : List<Command>,
                   events : List<Event>,
                   states : List<State>) {
    val allCommands = commands.map { it.name to it }.toMap()
    val allEvents  = events.map { it.name to it }.toMap()
    val allStates = states.map { it.name to it }.toMap()
    private val startState = allStates[startStateName]
    private val resetEvents = resetEventNames.map { allEvents[it] }.toSet()

    init {
        // resolve the state, event and command references
        allStates.values.forEach { it.resolve(this) }
    }

    fun command(name : String) = allCommands[name]!!
    fun event(name : String) = allEvents[name]!!
    fun state(name : String) = allStates[name]!!

    fun resetTarget(event : Event) =
        if (resetEvents.contains(event))
            startState
        else null
}

class CommandChannel {
    fun send(action: Command) {
        println("${action.name} / ${action.code}")
    }
}

class Controller(private var currentState : State,
                 private val stateMachine : StateMachine,
                 private val commandChannel : CommandChannel) {
    fun handle(event : Event) {
        val nextState =
                currentState.transitions[event] ?:
                stateMachine.resetTarget(event)
        nextState?.let {
            currentState = it
            currentState.actions.forEach { commandChannel.send(it) }
        }
    }
}