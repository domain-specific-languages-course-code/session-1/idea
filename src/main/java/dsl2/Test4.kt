package dsl2

// Adding some helper methods to improve readability
// The fluent constructor invocation becomes more readable, less noise
// But this still looks like kotlin function calls...
fun resetEventNames(vararg eventNames : String) = eventNames.toSet()
fun commands(vararg commands : Command) = commands.toList()
fun events(vararg events : Event) = events.toList()
fun states(vararg states : State) = states.toList()
fun transitions(vararg pairs : Pair<String, String>) = pairs.toMap()
fun actions(vararg actions : String) = actions.toList()
fun noActions() = emptyList<String>()


class Test4 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            // CONFIGURATION
            val stateMachine = StateMachine("idleState",
                    resetEventNames("doorOpened"),
                    commands(
                            Command("unlockPanel", "PNUL"),
                            Command("lockPanel", "PNLK"),
                            Command("lockDoor", "D1LK"),
                            Command("unlockDoor", "D1UL")
                    ),

                    events(
                            Event("doorClosed", "D1CL"),
                            Event("drawerOpened", "D2OP"),
                            Event("lightOn", "L1ON"),
                            Event("doorOpened", "D1OP"),
                            Event("panelClosed", "PNCL")
                    ),

                    states(
                            State("idleState",
                                    actions("unlockDoor", "lockPanel"),
                                    transitions(
                                            "doorClosed" to "activeState"
                                    )
                            ),
                            State("activeState",
                                    noActions(),
                                    transitions(
                                            "drawerOpened" to "waitingForLightState",
                                            "lightOn" to "waitingForDrawerState"
                                    )
                            ),
                            State("waitingForLightState",
                                    noActions(),
                                    transitions(
                                            "lightOn" to "unlockedPanelState"
                                    )
                            ),
                            State("waitingForDrawerState",
                                    noActions(),
                                    transitions(
                                            "drawerOpened" to "unlockedPanelState"
                                    )
                            ),
                            State("unlockedPanelState",
                                    actions("unlockPanel", "lockDoor"),
                                    transitions(
                                            "panelClosed" to "idleState"
                                    ))
                    ))


            fun e(name : String) = stateMachine.event(name)
            fun s(name : String) = stateMachine.state(name)

            // test run
            val controller = Controller(s("idleState"), stateMachine, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("lightOn"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 2")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 3")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 4")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 5")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("doorOpened"))
        }
    }
}