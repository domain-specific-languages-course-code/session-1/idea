package dsl2

// example of fluent constructor invocation
class Test3 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            // CONFIGURATION
            val stateMachine = StateMachine("idleState",
                    resetEventNames = setOf("doorOpened"),
                    commands = listOf(
                        Command("unlockPanel", "PNUL"),
                        Command("lockPanel", "PNLK"),
                        Command("lockDoor", "D1LK"),
                        Command("unlockDoor", "D1UL")
                    ),

                    events = listOf(
                        Event("doorClosed", "D1CL"),
                        Event("drawerOpened", "D2OP"),
                        Event("lightOn", "L1ON"),
                        Event("doorOpened", "D1OP"),
                        Event("panelClosed", "PNCL")
                    ),

                    states = listOf(
                        State("idleState",
                                listOf("unlockDoor", "lockPanel"),
                                mapOf("doorClosed" to "activeState")
                        ),
                        State("activeState",
                                listOf(),
                                mapOf(
                                    "drawerOpened" to "waitingForLightState",
                                    "lightOn" to "waitingForDrawerState"
                                )
                        ),
                        State("waitingForLightState",
                                listOf(),
                                mapOf(
                                    "lightOn" to "unlockedPanelState"
                                )
                        ),
                        State("waitingForDrawerState",
                                listOf(),
                                mapOf(
                                    "drawerOpened" to "unlockedPanelState"
                                )
                        ),
                        State("unlockedPanelState",
                                listOf("unlockPanel", "lockDoor"),
                                mapOf(
                                    "panelClosed" to "idleState"
                                ))
                    ))


            fun e(name : String) = stateMachine.event(name)
            fun s(name : String) = stateMachine.state(name)

            // test run
            val controller = Controller(s("idleState"), stateMachine, CommandChannel())

            println()
            println("TEST 1")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("lightOn"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 2")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("panelClosed"))

            println()
            println("TEST 3")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 4")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("doorOpened"))

            println()
            println("TEST 5")
            println("======")
            controller.handle(e("doorClosed"))
            controller.handle(e("lightOn"))
            controller.handle(e("drawerOpened"))
            controller.handle(e("doorOpened"))
        }
    }
}