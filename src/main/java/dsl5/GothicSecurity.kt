package dsl5
// back to the initial, direct object-based implementation
// for this example, we're generating the setup and test code using xText in Eclipse

abstract class AbstractEvent(val name : String,
                             val code : String)

class Command(name: String, code: String)
    : AbstractEvent(name, code)

class Event(name: String, code: String)
    : AbstractEvent(name, code)

class State {
    val actions = mutableListOf<Command>()
    val transitions = mutableMapOf<Event, State>()
}

class StateMachine(private val startState : State) {
    val resetEvents = mutableSetOf<Event>()
    val allEvents = mutableMapOf<String, Event>()
    val allStates = mutableMapOf<String, State>()

    fun event(name : String) = allEvents[name]!!
    fun state(name : String) = allStates[name]!!

    fun resetTarget(event : Event) =
        if (resetEvents.contains(event))
            startState
        else null
}

class CommandChannel {
    fun send(action: Command) {
        println("${action.name} / ${action.code}")
    }
}

class Controller(private var currentState : State,
                 private val stateMachine : StateMachine,
                 private val commandChannel : CommandChannel) {
    fun handle(event : Event) {
        val nextState =
                currentState.transitions[event] ?:
                stateMachine.resetTarget(event)
        nextState?.let {
            currentState = it
            currentState.actions.forEach { commandChannel.send(it) }
        }
    }
}