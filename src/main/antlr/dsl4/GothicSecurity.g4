grammar GothicSecurity;
@header {
    package dsl4;
}
//@lexer::header {package org.xmlcml.cml.converters.antlr;}

stateMachine
    :   'stateMachine' ID '{'
            'start' ID
            command*
            event*
            state*
            resetOn*
        '}'
    ;

command
    :   'command' ID 'code' ID
    ;

event
    :   'event' ID 'code' ID
    ;

state
    :   'state' ID '{'
            action*
            transition*
        '}'
    ;

action
    :   'action' ID
    ;

transition
    :   'on' ID 'switch' 'to' ID
    ;

resetOn
    :   'reset' 'on' ID
    ;

ID
    :   [a-zA-Z] [0-9a-zA-Z]*
    ;

WS  :   [ \t\n\r]+ -> skip ;
